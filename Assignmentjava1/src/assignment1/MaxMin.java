package assignment1;

import java.util.Arrays;
import java.util.Scanner;

public class MaxMin {

public static void main(String[] args)
{
	System.out.println("Enter the 5 integer numbers:");
    Scanner sc = new Scanner(System.in);
    int array[] = new int[5];
    int sum = 0;
    
    for(int i=0;i<5;i++)
    {
        array[i] = sc.nextInt();
        sum+= array[i];
    }
    Arrays.sort(array);
    int min = array[4];
    int max = array[0];
    int minSum = sum-min;
    int maxSum = sum-max;
    System.out.println("Minimum sum of integer:");
    System.out.println(minSum);
    System.out.println("Maximum sum of integer");
    System.out.println(maxSum);
                
   
    }
            
}